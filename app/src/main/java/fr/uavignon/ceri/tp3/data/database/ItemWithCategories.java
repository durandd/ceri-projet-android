package fr.uavignon.ceri.tp3.data.database;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class ItemWithCategories {
    @Embedded
    public Item item;
    @Relation(
            parentColumn = "item_id",
            entityColumn = "category_id",
            associateBy = @Junction(ItemCategoryCrossRef.class)
    )
    public List<Category> categories;
}

package fr.uavignon.ceri.tp3.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Item item);

    @Query("DELETE FROM item_table")
    void deleteAll();

    @Query("SELECT * from item_table ORDER BY name ASC")
    List<Item> getSynchrAllItems();

    @Query("SELECT * from item_table ORDER BY name ASC")
    LiveData<List<Item>> getAllItemsSortByName();

    @Query("SELECT * from item_table ORDER BY year ASC")
    LiveData<List<Item>> getAllItemsSortByYear();

    @Query("SELECT * FROM item_table WHERE item_id = :id")
    Item getItemById(String id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Item item);

    @Transaction
    @Query("SELECT * FROM item_table")
    public List<ItemWithCategories> getItemWithCategories();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertItemCategoryCrossRef(ItemCategoryCrossRef crossRef);
}

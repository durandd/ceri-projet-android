package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textItemName, textItemYear, textItemDesc, textItemTechnicalDetails, textItemBrand, textItemTimeFrame, textItemCategories;
    private ImageView imgItem;
    private ProgressBar progress;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        String ItemId = args.getItemId();
        viewModel.setItem(ItemId);
        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textItemName = getView().findViewById(R.id.item_name);
        textItemYear = getView().findViewById(R.id.item_year);
        textItemDesc = getView().findViewById(R.id.editDescription);
        textItemTechnicalDetails = getView().findViewById(R.id.editTechnicalDetails);
        textItemBrand = getView().findViewById(R.id.editBrand);
        textItemTimeFrame = getView().findViewById(R.id.editTimeFrame);
        textItemCategories = getView().findViewById(R.id.editCategories);

        imgItem = getView().findViewById(R.id.item_picture);


        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getItem().observe(getViewLifecycleOwner(),
                item -> {
                    if (item != null) {
                        Log.d(TAG, "observing item view");

                        textItemName.setText(item.getName());
                        if(item.getYear() != 0){
                            textItemYear.setText(String.valueOf(item.getYear()));
                        }

                        if (item.getDescription() != null)
                            textItemDesc.setText(item.getDescription());
                        if (item.getTechnicalDetailsList()!= null) {
                            String technicalDetails = "";
                            for (String s : item.getTechnicalDetailsList()) {
                                technicalDetails = technicalDetails + "\n" + s;
                            }
                            textItemTechnicalDetails.setText(technicalDetails);
                        }
                        if (item.getBrand() != null)
                            textItemBrand.setText(item.getBrand());
                        if (item.getTimeFrameList() != null){
                            String timeFrame = "";
                            for (Integer year : item.getTimeFrameList()) {
                                timeFrame = timeFrame + "[" + year.toString() + "] ";
                            }
                            textItemTimeFrame.setText(timeFrame);
                        }

                        //textItemCategories.setText(item.getStrLastUpdate());

                        // set ImgView
                        Log.d("DEBUG", item.getPictures());
                        if (item.getPicturesMap() != null)
                            loadImage(imgItem, viewModel.getPicturesUrl(item).get(0));

                    }
                });



    }

    private void loadImage(ImageView view, String url) {
        if (url != null) {
            Glide.with(view.getContext()).load(url).into(view);
        }
    }


}
package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.graphics.drawable.Drawable;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp3.data.database.Item;
import fr.uavignon.ceri.tp3.data.ItemRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private ItemRepository repository;
    private MutableLiveData<Item> item;

    public DetailViewModel (Application application) {
        super(application);
        repository = ItemRepository.get(application);
        item = new MutableLiveData<>();
    }

    public void setItem(String id) {
        repository.setSelectedItem(id);
        item = repository.getSelectedItem();
    }

    LiveData<Item> getItem() {
        return item;
    }

    public void loadCity(Item c){
        //repository.loadWeatherCity(c);
    }

    public List<String> getPicturesUrl(Item item) {
        return repository.getImagesUrl(item);
    }
}


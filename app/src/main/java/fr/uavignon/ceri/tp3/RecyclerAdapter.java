package fr.uavignon.ceri.tp3;


import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp3.data.database.Item;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.tp3.RecyclerAdapter.ViewHolder> {

    private static final String TAG = fr.uavignon.ceri.tp3.RecyclerAdapter.class.getSimpleName();

    private List<Item> itemList;
    private ListViewModel listViewModel;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.itemTitle.setText(itemList.get(i).getName());
        if(itemList.get(i).getBrand()!=null){
            viewHolder.itemDetail1.setText(itemList.get(i).getBrand());
        }
        // categories
        //viewHolder.itemDetail2.setText(String.valueOf(itemList.get(i).getYear()));
        String url = listViewModel.getThumbnail(itemList.get(i));
        if (url != null) {
            Glide.with(viewHolder.itemIcon.getContext()).load(url).into(viewHolder.itemIcon);
        }
    }

    @Override
    public int getItemCount() {
        //return Book.books.length;
        return itemList == null ? 0 : itemList.size();
    }

    public void setItemList(List<Item> cities) {
        itemList = cities;
        notifyDataSetChanged();
    }
    public void setListViewModel(ListViewModel viewModel) {
        listViewModel = viewModel;
    }

     class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail1;
        TextView itemDetail2;
        TextView itemTemp;
        ImageView itemIcon;

        ActionMode actionMode;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail1 = itemView.findViewById(R.id.item_detail1);
            itemDetail2 = itemView.findViewById(R.id.item_detail2);
            itemIcon = itemView.findViewById(R.id.item_thumbMail);



            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Log.d(TAG,"position="+getAdapterPosition());
                    String id = RecyclerAdapter.this.itemList.get((int)getAdapterPosition()).getItem_id();
                    ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment(id);
                    Navigation.findNavController(v).navigate(action);
                }
            });
        }

     }
}
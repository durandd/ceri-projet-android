package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface OWMInterface {

    @Headers("Accept: application/geo+json")
    @GET("/cerimuseum/collection")
    Call<Map<String, ItemResponse>> getCollection();


    @Headers("Accept: application/geo+json")
    @GET("/cerimuseum/categories")
    Call<List<String>> getCategories();


    @Headers("Accept: application/geo+json")
    @GET("/cerimuseum/items/{itemID}")
    Call<ItemResponse> getItem(@Path("itemID") String itemId);


    @Headers("Accept: application/geo+json")
    @GET("/cerimuseum/popularity")
    Call<Map<String, Integer>> getPopularity();


    @Headers("Accept: application/geo+json")
    @GET("/cerimuseum/collectionversion")
    Call<String> getCollectionVersion();


    @Headers("Accept: application/geo+json")
    @GET("/cerimuseum/popularityversion")
    Call<String> getPopularityVersion();


    @Headers("Accept: application/geo+json")
    @POST("/cerimuseum/items/{itemID}/like")
    Call<ResponseBody> addItemToFavorites(@Path("itemID") String itemId);


    @Headers("Accept: application/geo+json")
    @POST("/cerimuseum/items/{itemID}/like")
    Call<ResponseBody> removeItemFromFavorites(@Path("itemID") String itemId);
}
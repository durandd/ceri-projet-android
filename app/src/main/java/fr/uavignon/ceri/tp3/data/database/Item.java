package fr.uavignon.ceri.tp3.data.database;


import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Entity(tableName = "item_table")
public class Item {

    public static final String TAG = Item.class.getSimpleName();

    private static Moshi moshi = new Moshi.Builder().build();
    private static JsonAdapter<List<String>> stringListAdapter
            = moshi.adapter(Types.newParameterizedType(List.class, String.class));
    private static JsonAdapter<List<Integer>> intListAdapter
            = moshi.adapter(Types.newParameterizedType(List.class, Integer.class));
    private static JsonAdapter<Map<String, String>> mapAdapter
            = moshi.adapter(Types.newParameterizedType(Map.class, String.class, String.class));

    @PrimaryKey
    @NonNull
    @ColumnInfo(name="item_id")
    private String item_id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @ColumnInfo(name="brand")
    private String brand;

    @ColumnInfo(name="year")
    private int year;

    @ColumnInfo(name="timeFrame")
    private String timeFrame;

    @ColumnInfo(name="description")
    private String description;

    @ColumnInfo(name="technicalDetails")
    private String technicalDetails;

    @ColumnInfo(name="pictures")
    private String pictures;

    @ColumnInfo(name="popularity")
    private int popularity;

    @ColumnInfo(name="favorite")
    private boolean favorite;

    public Item(@NonNull String item_id, @NonNull String name, String brand, int year,
                String timeFrame, String description, String technicalDetails, String pictures,
                int popularity, boolean favorite) {
        this.item_id = item_id;
        this.name = name;
        this.brand = brand;
        this.year = year;
        this.timeFrame = timeFrame;
        this.description = description;
        this.technicalDetails = technicalDetails;
        this.pictures = pictures;
        this.popularity = popularity;
        this.favorite = favorite;
    }

    @NonNull
    public String getItem_id() {
        return item_id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public int getYear() {
        return year;
    }

    public List<Integer> getTimeFrameList() {
        try {
            return intListAdapter.fromJson(timeFrame);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getTechnicalDetailsList() {
        try {
            return stringListAdapter.fromJson(technicalDetails);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Map<String, String> getPicturesMap() {
        try {
            return mapAdapter.fromJson(pictures);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("JSON", "empty map");
        }
        return null;
    }

    public int getPopularity() {
        return popularity;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public String getTimeFrame() {
        return timeFrame;
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }

    public String getPictures() {
        return pictures;
    }

    @Override
    public String toString() {
        return null;
    }


}

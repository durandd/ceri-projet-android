package fr.uavignon.ceri.tp3.data.database;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "category_table")
public class Category {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="category_id")
    private long category_id;

    @NonNull
    @ColumnInfo(name="category_name")
    private String name;


    public Category(@NonNull String name) {
        this.name = name;
    }

    public long getCategory_id() {
        return category_id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setCategory_id(long category_id) {
        this.category_id = category_id;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }
}

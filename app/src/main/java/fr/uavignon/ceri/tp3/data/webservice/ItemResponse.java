package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;
import java.util.Map;

public class ItemResponse {


    public final String name;
    public final int year;
    public final String brand;
    public final String description;
    public final boolean working;
    public final List<Integer> timeFrame;
    public final List<String> technicalDetails;
    public final List<String> categories;
    public final Map<String,String> pictures;


    public ItemResponse(String name, int year, String brand, String description, boolean working,
                        List<Integer> timeFrame, List<String> technicalDetails, List<String> categories,
                        Map<String, String> pictures) {
        this.name = name;
        this.year = year;
        this.brand = brand;
        this.description = description;
        this.working = working;
        this.timeFrame = timeFrame;
        this.technicalDetails = technicalDetails;
        this.categories = categories;
        this.pictures = pictures;
    }
}

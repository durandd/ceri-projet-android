package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.graphics.drawable.Drawable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import java.util.List;
import fr.uavignon.ceri.tp3.data.database.Item;
import fr.uavignon.ceri.tp3.data.ItemRepository;

public class ListViewModel extends AndroidViewModel {
    private ItemRepository repository;
    private LiveData<List<Item>> allItems;
    private final MediatorLiveData<List<Item>> results = new MediatorLiveData<>();

    public ListViewModel (Application application) {
        super(application);
        repository = ItemRepository.get(application);
        allItems = repository.getItemsSortByName();
    }

    public void sortListbyName(){
        allItems = repository.getItemsSortByName();
    }

    public void sortListbyYear(){
        allItems = repository.getItemsSortByYear();
    }

    LiveData<List<Item>> getAllItems() {
        return allItems;
    }

    public void updateCollection(){
        repository.updateDatabase();
    }

    public String getThumbnail(Item item) {
        return repository.getThumbmailUrl(item);
    }
}

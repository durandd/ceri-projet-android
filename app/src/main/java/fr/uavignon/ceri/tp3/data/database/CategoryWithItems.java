package fr.uavignon.ceri.tp3.data.database;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

public class CategoryWithItems {
    @Embedded
    public Category category;
    @Relation(
            parentColumn = "category_id",
            entityColumn = "item_id",
            associateBy = @Junction(ItemCategoryCrossRef.class)
    )
    public List<Item> items;
}

package fr.uavignon.ceri.tp3.data.database;

import androidx.annotation.NonNull;
import androidx.room.Entity;

@Entity(primaryKeys = {"item_id", "category_id"})
public class ItemCategoryCrossRef {

    @NonNull
    public String item_id;

    @NonNull
    public long category_id;


    public ItemCategoryCrossRef(String item_id, long category_id) {
        this.item_id = item_id;
        this.category_id = category_id;
    }
}

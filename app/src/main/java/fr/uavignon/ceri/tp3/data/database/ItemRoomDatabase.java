package fr.uavignon.ceri.tp3.data.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(
        entities = {
                Item.class,
                Category.class,
                ItemCategoryCrossRef.class
        },
        version = 1, exportSchema = false)
public abstract class ItemRoomDatabase extends RoomDatabase {

    private static final String TAG = ItemRoomDatabase.class.getSimpleName();

    public abstract ItemDao itemDao();
    public abstract CategoryDAO categoryDAO();

    private static ItemRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static ItemRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (ItemRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                            // without populate
                        /*
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    WeatherRoomDatabase.class,"book_database")
                                    .build();

                     */

                            // with populate
                            INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    ItemRoomDatabase.class,"item_database")
                                    .addCallback(sRoomDatabaseCallback)
                                    .build();

                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        ItemDao itemDao = INSTANCE.itemDao();
                        CategoryDAO categoryDAO = INSTANCE.categoryDAO();
                        itemDao.deleteAll();
                        categoryDAO.deleteAll();
                        Log.d(TAG,"database populated");
                    });

                }
            };



}

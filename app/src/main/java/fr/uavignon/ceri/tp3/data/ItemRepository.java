package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import fr.uavignon.ceri.tp3.data.database.Category;
import fr.uavignon.ceri.tp3.data.database.CategoryDAO;
import fr.uavignon.ceri.tp3.data.database.Item;
import fr.uavignon.ceri.tp3.data.database.ItemCategoryCrossRef;
import fr.uavignon.ceri.tp3.data.database.ItemDao;
import fr.uavignon.ceri.tp3.data.database.ItemRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.ItemResponse;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.ItemRoomDatabase.databaseWriteExecutor;

public class ItemRepository {

    private static final String URL = "https://demo-lia.univ-avignon.fr" ;
    private static final String TAG = ItemRepository.class.getSimpleName();

    private LiveData<List<Category>> allCategories;
    private MutableLiveData<Item> selectedItem = new MutableLiveData<Item>();;

    private ItemDao itemDao;
    private CategoryDAO categoryDAO;

    private OWMInterface api;

    private static volatile ItemRepository INSTANCE;

    private Moshi moshi;
    private JsonAdapter<List<String>> stringListAdapter;
    private JsonAdapter<List<Integer>> intListAdapter;
    private JsonAdapter<Map<String, String>> mapAdapter;

    public synchronized static ItemRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new ItemRepository(application);
        }

        return INSTANCE;
    }

    public ItemRepository(Application application) {

        moshi = new Moshi.Builder().build();
        stringListAdapter = moshi.adapter(Types.newParameterizedType(List.class, String.class));
        intListAdapter = moshi.adapter(Types.newParameterizedType(List.class, Integer.class));
        mapAdapter = moshi.adapter(Types.newParameterizedType(Map.class, String.class, String.class));

        ItemRoomDatabase db = ItemRoomDatabase.getDatabase(application);
        itemDao = db.itemDao();
        categoryDAO = db.categoryDAO();

        Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(URL)
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();
        api = retrofit.create(OWMInterface.class);

    }

    public LiveData<List<Item>> getItemsSortByName() {
        return itemDao.getAllItemsSortByName();
    }

    public LiveData<List<Item>> getItemsSortByYear() {
        return itemDao.getAllItemsSortByYear();
    }

    public MutableLiveData<Item> getSelectedItem() {
        return selectedItem;
    }


    public void updateDatabase(){
        if(!upToDate()){
            //remove favorites

            deleteAll();

            api.getCollection().enqueue(
                    new Callback<Map<String, ItemResponse>>() {
                        @Override
                        public void onResponse(Call<Map<String, ItemResponse>> call,
                                               Response<Map<String, ItemResponse>> response) {
                            if(response.isSuccessful()){
                                Set<String> categories = new HashSet<String>();
                                for (String item_id : response.body().keySet()) {
                                    ItemResponse itemResponse = response.body().get(item_id);
                                    Item item = new Item(
                                            item_id,
                                            itemResponse.name,
                                            itemResponse.brand,
                                            itemResponse.year,
                                            intListAdapter.toJson(itemResponse.timeFrame),
                                            itemResponse.description,
                                            stringListAdapter.toJson(itemResponse.technicalDetails),
                                            mapAdapter.toJson(itemResponse.pictures),
                                            0,
                                            false
                                    );
                                    insertItem(item);
                                    for (String c: itemResponse.categories ) {
                                        Category category = new Category(c);
                                        long category_id = insertCategory(category);
                                        insertItemCategoryCrossRef(
                                                new ItemCategoryCrossRef(
                                                        item_id,
                                                        category_id
                                                )
                                        );
                                    }
                                }
                            }

                            else {
                                try {
                                    Log.d("QUERY",response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {
                            Log.d("QUERY","Faillure");
                        }
                    });

        }
    }

    private boolean upToDate() {
        return false;
    }

    public void deleteAll() {
        databaseWriteExecutor.execute(() -> {
            itemDao.deleteAll();
            categoryDAO.deleteAll();
        });

    }

    public long insertItem(Item newItem) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return itemDao.insert(newItem);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }

    public long insertCategory(Category category) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return categoryDAO.insert(category);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }

    public long insertItemCategoryCrossRef(ItemCategoryCrossRef crossRef) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return itemDao.insertItemCategoryCrossRef(crossRef);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return res;
    }

    public int updateItem(Item item) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return itemDao.update(item);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedItem.setValue(item);
        return res;
    }

    public void setSelectedItem(String id)  {
        Future<Item> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return itemDao.getItemById(id);
        });
        try {
            selectedItem.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public String getThumbmailUrl(Item item) {
        return URL + "/cerimuseum/items/" + item.getItem_id() +"/thumbnail";
    }

    public List<String> getImagesUrl(Item item) {
        List<String> urls = new ArrayList<String>();
        for (String imageId: item.getPicturesMap().keySet()) {
            urls.add(URL + "/cerimuseum/items/" + item.getItem_id() +"/images/" + imageId);
        }
        return urls;

    }
}

package fr.uavignon.ceri.tp3.data.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import java.util.List;

@Dao
public interface CategoryDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Category category);

    @Query("DELETE FROM category_table")
    void deleteAll();

    @Query("SELECT * from category_table ORDER BY category_name ASC")
    List<Category> getAllCategories();

    @Query("DELETE FROM category_table WHERE category_id = :id")
    void deleteCity(int id);

    @Query("SELECT * FROM category_table WHERE category_id = :id")
    Category getCityById(int id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Item item);

    @Transaction
    @Query("SELECT * FROM category_table")
    public List<CategoryWithItems> getCategoryWithItems();
}
